Categories:Security,Internet
License:GPLv3
Web Site:
Source Code:https://github.com/M66B/NetGuard
Issue Tracker:https://github.com/M66B/NetGuard/issues

Auto Name:NetGuard
Summary:Block network access
Description:
Simple way to block access to the internet - no root required. Applications can
individually be allowed or denied access via your Wi-Fi and/or mobile
connection.
.

Repo Type:git
Repo:https://github.com/M66B/NetGuard

Build:0.7,7
    commit=42ad6c2c8137cb82f73fa2efd5e5f47959f92f17
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.12,12
    commit=0.12
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.13,13
    commit=0.13
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.14,14
    commit=0.14
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.16,16
    commit=0.16
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.17,17
    commit=0.17
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.18,18
    commit=0.18
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.19,19
    commit=0.19
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.21,21
    commit=0.21
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.24,24
    commit=0.24
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.28,28
    commit=0.28
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.29,29
    commit=0.29
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Build:0.32,32
    commit=0.32
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationVariants/,+2d' -e '/def setOutputFile/i/*' -e '/dependencies {/i*/' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags ^[0-9.]*$
Current Version:0.32
Current Version Code:32
