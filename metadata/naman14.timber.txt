Categories:Multimedia
License:GPLv3+
Web Site:https://github.com/naman14/Timber/blob/HEAD/README.md
Source Code:https://github.com/naman14/Timber
Issue Tracker:https://github.com/naman14/Timber/issues
Changelog:https://github.com/naman14/Timber/blob/HEAD/Changelog.md

Auto Name:Timber
Summary:Material Design Music Player
Description:
Timber is a Music Player currently in Beta
.

Repo Type:git
Repo:https://github.com/naman14/Timber

Build:0.11b,3
    commit=9ae127610c18e22716ad4d07471e3955fd11f347
    subdir=app
    gradle=yes

Build:0.122b,6
    commit=f95512a84b2876e3afe7101f0cb1ef49ce829c20
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.122b
Current Version Code:6
