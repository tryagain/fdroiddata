Categories:Internet
License:GPLv3
Web Site:https://github.com/dimtion/Shaarlier/blob/HEAD/README.md
Source Code:https://github.com/dimtion/Shaarlier
Issue Tracker:https://github.com/dimtion/Shaarlier/issues

Auto Name:Shaarlier
Summary:Share links on Shaarli
Description:
Easily share links on Shaarli instances.

Features:

* Publish links on your Shaarli
* Automatically add a title to your links (as Shaarli does)
* Optional dialog box for editing title, description or tags
* Multiple Shaarlis
.

Repo Type:git
Repo:https://github.com/dimtion/Shaarlier

Build:1.2.0,13
    commit=v1.2.0
    subdir=app
    gradle=yes

Build:1.2.0-debug,14
    commit=v1.2.0-debug
    subdir=app
    gradle=yes

Build:1.2.2,16
    commit=v1.2.2
    subdir=app
    gradle=yes

Build:1.2.3,17
    commit=v1.2.3
    subdir=app
    gradle=yes

Build:1.2.4,18
    commit=v1.2.4
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2.4
Current Version Code:18
