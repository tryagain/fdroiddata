Categories:System,Development
License:GPLv3
Web Site:http://termux.com
Source Code:https://github.com/termux/termux-api
Issue Tracker:https://github.com/termux/termux-api/issues

Auto Name:Termux:API
Summary:Access Android functions from Termux
Description:
Expose basic Android functionality like sending SMS or accessing GPS data to the
[[com.termux]] app.
.

Repo Type:git
Repo:https://github.com/termux/termux-api

Build:0.4,4
    disable=remove apk
    commit=v0.4
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/signingConfig/d' build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.4
Current Version Code:4
